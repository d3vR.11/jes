import React, { useState, useEffect, useRef } from 'react';
import classNames from 'classnames';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import FiscaliasService from './FiscaliasService';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { Toolbar } from 'primereact/toolbar';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';
import './DataTable.css';

const App = () =>{
  let emptyFiscalia = {
    id: null,
    nombre: '',
    direccion: '',
    telefono: 0,
    departamento: {
      id: 0,
      nombre: ''
    },
    municipio: {
      id: 0,
      nombre: ''
    },
    aldea: {
      id: 0,
      nombre: ''
    },
    tipo: {
      id: 0,
      nombre: ''
    }
  };

  const [fiscalias, setFiscalias] = useState(null);
  const [productDialog, setProductDialog] = useState(false);
  const [deleteProductDialog, setDeleteProductDialog] = useState(false);
  const [fiscalia, setFiscalia] = useState(emptyFiscalia);
  const [selectedProducts, setSelectedProducts] = useState(null);
  const [submitted, setSubmitted] = useState(false);
  const [globalFilter, setGlobalFilter] = useState(null);
  const toast = useRef(null);
  const dt = useRef(null);
  const [selectedTipo, setSelectedTipo] = useState(null);
  const [tiposSelectItems, setTiposSelectItems] = useState(null);
  const [selectedDepto, setSelectedDepto] = useState(null);
  const [deptoSelectItems, setDeptoSelectItems] = useState(null);
  const [selectedMuni, setSelectedMuni] = useState(null);
  const [muniSelectItems, setMuniSelectItems] = useState(null);
  const [selectedAldea, setSelectedAldea] = useState(null);
  const [aldeaSelectItems, setAldeaSelectItems] = useState(null);
  const productService = new FiscaliasService();
  
  const onTipoChange = (e) => {
    let _fiscalia = {...fiscalia};
    _fiscalia['tipo']['id'] = e.value;
    setFiscalia(_fiscalia);
    setSelectedTipo(e.value);
  }

  const onDeptoChange = (e) => {
    let _fiscalia = {...fiscalia};
    _fiscalia['departamento']['id'] = e.value;
    setFiscalia(_fiscalia);
    setSelectedDepto(e.value);
  }

  const onMuniChange = (e) => {
    let _fiscalia = {...fiscalia};
    _fiscalia['municipio']['id'] = e.value;
    setFiscalia(_fiscalia);
    setSelectedMuni(e.value);
  }
  
  const onAldeaChange = (e) => {
    let _fiscalia = {...fiscalia};
    _fiscalia['aldea']['id'] = e.value;
    setFiscalia(_fiscalia);
    setSelectedAldea(e.value);
  }

  useEffect(() => {
      productService.getFiscalias().then(function (data) {
        setFiscalias(data);
      });

      productService.getTiposFiscalias().then(function (data) {
        setTiposSelectItems(data);
      });
    
      productService.getDepartamentos().then(function (data){
        setDeptoSelectItems(data);
      });
    
      productService.getMunicipios().then(function (data){
        setMuniSelectItems(data);
      });

      productService.getAldeas().then(function (data){
        setAldeaSelectItems(data);
      });
  }, []);// eslint-disable-line react-hooks/exhaustive-deps

  const openNew = () => {
      setFiscalia(emptyFiscalia);
      setSelectedTipo(0);
      setSelectedDepto(0);
      setSelectedMuni(0);
      setSelectedAldea(0);
      setSubmitted(false);
      setProductDialog(true);
  }

  const hideDialog = () => {
      setSubmitted(false);
      setProductDialog(false);
  }

  const hideDeleteProductDialog = () => {
      setDeleteProductDialog(false);
  }

  const saveProduct = () => {
      setSubmitted(true);
      if (fiscalia.nombre.trim()) {
          if (fiscalia.id) {
              productService.updateFiscalia(fiscalia);
              toast.current.show({ severity: 'success', summary: 'Exito', detail: 'Fiscalia Actualizada', life: 3000 });
          }
          else {
              productService.createFiscalia(fiscalia);
              toast.current.show({ severity: 'success', summary: 'Exito', detail: 'Fiscalia Creada', life: 3000 });
          }

          productService.getFiscalias().then(function (data) {
            setFiscalias(data);
          });
          setProductDialog(false);
          setFiscalia(emptyFiscalia);
      }
  }

  const editProduct = (fiscalia) => {
      setFiscalia({...fiscalia});
      setSelectedMuni(fiscalia.municipio.id);
      setSelectedDepto(fiscalia.departamento.id);
      setSelectedAldea(fiscalia.aldea.id);
      setSelectedTipo(fiscalia.tipo.id);
      setProductDialog(true);
  }

  const confirmDeleteProduct = (fiscalia) => {
      setFiscalia(fiscalia);
      setDeleteProductDialog(true);
  }

  const deleteProduct = () => {
      productService.deleteFiscalia(fiscalia.id, 200815460);
      productService.getFiscalias().then(function (data) {
        setFiscalias(data);
      });

      setDeleteProductDialog(false);
      setFiscalia(emptyFiscalia);
      toast.current.show({ severity: 'success', summary: 'Exito', detail: 'Fiscalia Eliminada', life: 3000 });
  }

  const onInputChange = (e, name) => {
      const val = (e.target && e.target.value) || '';
      let _product = {...fiscalia};
      _product[`${name}`] = val;

      setFiscalia(_product);
  }

  const leftToolbarTemplate = () => {
      return (
          <React.Fragment>
              <Button label="Nueva Fiscalia" icon="pi pi-plus" className="p-button-success p-mr-2" onClick={openNew} />
          </React.Fragment>
      )
  }

  const actionBodyTemplate = (rowData) => {
      return (
          <React.Fragment>
              <Button icon="pi pi-pencil" className="p-button-rounded p-button-success p-mr-2" onClick={() => editProduct(rowData)} />
              <Button icon="pi pi-trash" className="p-button-rounded p-button-warning" onClick={() => confirmDeleteProduct(rowData)} />
          </React.Fragment>
      );
  }

  const header = (
      <div className="table-header">
          <h5 className="p-m-0">Fiscalias Disponibles</h5>
          <span className="p-input-icon-left">
              <i className="pi pi-search" />
              <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Buscar..." />
          </span>
      </div>
  );
  const productDialogFooter = (
      <React.Fragment>
          <Button label="Cancelar" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
          <Button label="Guardar" icon="pi pi-check" className="p-button-text" onClick={saveProduct} />
      </React.Fragment>
  );
  const deleteProductDialogFooter = (
      <React.Fragment>
          <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hideDeleteProductDialog} />
          <Button label="Si" icon="pi pi-check" className="p-button-text" onClick={deleteProduct} />
      </React.Fragment>
  );

  return (
      <div className="datatable-crud-demo">
          <Toast ref={toast} />

          <div className="card">
              <Toolbar className="p-mb-4" left={leftToolbarTemplate}></Toolbar>

              <DataTable ref={dt} value={fiscalias} selection={selectedProducts} onSelectionChange={(e) => setSelectedProducts(e.value)}
                  dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]}
                  paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink RowsPerPageDropdown"
                  currentPageReportTemplate="Mostrando {first} a {last} de {totalRecords} fiscalias"
                  globalFilter={globalFilter}
                  header={header}>

                  <Column field="id" header="Id" sortable></Column>
                  <Column field="nombre" header="Nombre" sortable></Column>
                  <Column field="direccion" header="Direccion" sortable></Column>
                  <Column field="telefono" header="Telefono" sortable></Column>
                  <Column field="departamento.nombre" header="Depto"></Column>
                  <Column field="municipio.nombre" header="Municipio"></Column>
                  <Column field="aldea.nombre" header="Aldea"></Column>
                  <Column field="tipo.nombre" header="Tipo"></Column>
                  <Column body={actionBodyTemplate}></Column>
              </DataTable>
          </div>

          <Dialog visible={productDialog} style={{ width: '500px' }} header="Detalle de Fiscalia" modal className="p-fluid" footer={productDialogFooter} onHide={hideDialog}>
              <div className="p-field">
                  <label htmlFor="nombre">Nombre</label>
                  <InputText id="nombre" value={fiscalia.nombre} onChange={(e) => onInputChange(e, 'nombre')} required autoFocus className={classNames({ 'p-invalid': submitted && !fiscalia.nombre })} />
                  {submitted && !fiscalia.nombre && <small className="p-error">El Nombre es requerido.</small>}
              </div>
              <div className="p-field">
                  <label htmlFor="direccion">Direccion</label>
                  <InputText id="direccion" value={fiscalia.direccion} onChange={(e) => onInputChange(e, 'direccion')} required />
              </div>
              <div className="p-field">
                  <label htmlFor="telefono">Telefono</label>
                  <InputText id="telefono" value={fiscalia.telefono} onChange={(e) => onInputChange(e, 'telefono')} required className={classNames({ 'p-invalid': submitted && !fiscalia.telefono })} />
                  {submitted && !fiscalia.telefono && <small className="p-error">El Nombre es requerido.</small>}
              </div>

              <div className="p-field">
                  <label className="p-mb-3">Tipo</label>
                  <div className="p-formgrid p-grid">
                    <Dropdown value={selectedTipo} options={tiposSelectItems} onChange={onTipoChange} optionLabel="nombre" optionValue="id" placeholder="Seleccione un tipo" />
                  </div>
              </div>

              <div className="p-formgrid p-grid">
                  <div className="p-field p-col">
                    <label className="p-mb-3">Departamento</label>
                    <div className="p-formgrid p-grid">
                      <Dropdown value={selectedDepto} options={deptoSelectItems} onChange={onDeptoChange} optionLabel="nombre" optionValue="id" placeholder="Seleccione un Departamento" />
                    </div>
                  </div>
                  <div className="p-field p-col">
                    <label className="p-mb-3">Municipio</label>
                    <div className="p-formgrid p-grid">
                      <Dropdown value={selectedMuni} options={muniSelectItems} onChange={onMuniChange} optionLabel="nombre" optionValue="id" placeholder="Seleccione un Municipio" />
                    </div>
                  </div>
                  <div className="p-field p-col">
                    <label className="p-mb-3">Aldea</label>
                    <div className="p-formgrid p-grid">
                      <Dropdown value={selectedAldea} options={aldeaSelectItems} onChange={onAldeaChange} optionLabel="nombre" optionValue="id" placeholder="Seleccione una Aldea" />
                    </div>
                  </div>
              </div>
          </Dialog>

          <Dialog visible={deleteProductDialog} style={{ width: '450px' }} header="Confirmar" modal footer={deleteProductDialogFooter} onHide={hideDeleteProductDialog}>
              <div className="confirmation-content">
                  <i className="pi pi-exclamation-triangle p-mr-3" style={{ fontSize: '2rem'}} />
                  {fiscalia && <span>Esta seguro de Eliminar <b>{fiscalia.nombre}</b>?</span>}
              </div>
          </Dialog>
      </div>
  );
}

export default App;
