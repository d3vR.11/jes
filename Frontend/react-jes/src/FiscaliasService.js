import axios from 'axios';

export default class FiscaliasService {
    getFiscalias() {
        return axios.get('http://localhost:8080/jes_ws/resources/api/getFiscalias').then(response => response.data);
        //return axios.get('data/fiscalias.json').then(response => response.data.data);
    }

    getTiposFiscalias() {
        return axios.get('http://localhost:8080/jes_ws/resources/api/getTiposFiscalias').then(response => response.data);
    }

    getDepartamentos() {
        return axios.get('http://localhost:8080/jes_ws/resources/api/getDepartamentos').then(response => response.data);
    }

    getMunicipios() {
        return axios.get('http://localhost:8080/jes_ws/resources/api/getMunicipios').then(response => response.data);
    }

    getAldeas() {
        return axios.get('http://localhost:8080/jes_ws/resources/api/getAldeas').then(response => response.data);
    }

    createFiscalia(fiscalia) {
        const params = new URLSearchParams()
        params.append('nombre', fiscalia.nombre)
        params.append('direccion', fiscalia.direccion)
        params.append('telefono', fiscalia.telefono)
        params.append('idDepartamento', fiscalia.departamento.id)
        params.append('idMunicipio', fiscalia.municipio.id)
        params.append('idAldea', fiscalia.aldea.id)
        params.append('idTipo', fiscalia.tipo.id)
        params.append('usuario', 200815460)

        const config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }

        return axios.post('http://localhost:8080/jes_ws/resources/api/nuevaFiscalia', params, config).then(response => response.data);
    }

    updateFiscalia(fiscalia) {
        const params = new URLSearchParams()
        params.append('idFiscalia', fiscalia.id)
        params.append('nombre', fiscalia.nombre)
        params.append('direccion', fiscalia.direccion)
        params.append('telefono', fiscalia.telefono)
        params.append('idDepartamento', fiscalia.departamento.id)
        params.append('idMunicipio', fiscalia.municipio.id)
        params.append('idAldea', fiscalia.aldea.id)
        params.append('idTipo', fiscalia.tipo.id)
        params.append('usuario', 200815460)

        const config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }

        return axios.post('http://localhost:8080/jes_ws/resources/api/actualizarFiscalia', params, config).then(response => response.data);
    }

    deleteFiscalia(idFiscalia, usuario){
        return axios.delete('http://localhost:8080/jes_ws/resources/api/borrarFiscalia/' + idFiscalia + '/' + usuario);
    }
}