INSERT INTO departamento(nombre)
values('Guatemala'),
('Escuintla'),
('Alta Verapaz'),
('Baja Verapaz'),
('Peten'),
('Izabal'),
('El Progreso'),
('Chiquimula');


INSERT INTO municipio(nombre, id_departamento)
values('Amatitlan', 1),
('Villa Nueva', 1),
('Mixco', 1),
('Chinautla', 1),
('Tiquisate', 2),
('Nueva Concepcion', 2),
('Masagua', 2),
('Iztapa', 2);

INSERT INTO aldea(nombre, id_departamento, id_municipio)
values('El Cerrito', 1, 1),
('Las Trojes', 1, 1),
('San Carlos', 1, 1),
('Mesillas Bajas', 1, 1),
('Agua de las Minas', 1, 1);

INSERT INTO tipo_fiscalia(nombre)
values('Municipal'),
('Distrital'),
('Asuntos Internos'),
('Contra Secuestros'),
('Crimen Organizado');