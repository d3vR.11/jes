CREATE TABLE departamento (
    id INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
    nombre NVARCHAR(200) NOT NULL
);

CREATE TABLE municipio(
    id INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
    nombre NVARCHAR(299) NOT NULL,
    id_departamento INT NOT NULL FOREIGN KEY REFERENCES departamento(id)
);

CREATE TABLE aldea(
    id INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
    nombre NVARCHAR(200) NOT NULL,
    id_departamento INT NOT NULL FOREIGN KEY REFERENCES departamento(id),
    id_municipio INT NOT NULL FOREIGN KEY REFERENCES municipio(id)
);


CREATE TABLE tipo_fiscalia(
    id INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
    nombre NVARCHAR(75)
);

CREATE TABLE fiscalia(
    id INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
    nombre NVARCHAR(200) NOT NULL,
    direccion NVARCHAR(500) NOT NULL,
    telefono NVARCHAR(7) NULL,
    id_departamento INT NOT NULL FOREIGN KEY REFERENCES departamento(id),
    id_municipio INT NOT NULL FOREIGN KEY REFERENCES municipio(id),
    id_aldea INT NOT NULL FOREIGN KEY REFERENCES aldea(id),
    tipo INT NOT NULL FOREIGN KEY REFERENCES tipo_fiscalia(id)
);


CREATE TABLE bitacora_fiscalia(
    id INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
    id_fiscalia INT,
    nombre NVARCHAR(200),
    direccion NVARCHAR(500),
    telefono NVARCHAR(7),
    id_departamento INT,
    id_municipio INT,
    id_aldea INT,
    tipo INT,
    usuario INT NOT NULL,
    fecha datetime NOT NULL,
    accion NVARCHAR(50) NOT NULL
);






CREATE PROCEDURE nueva_fiscalia 
    @nombre NVARCHAR(200), 
    @direccion NVARCHAR(500), 
    @telefono NVARCHAR(7), 
    @id_departamento int, 
    @id_municipio int, 
    @id_aldea int, 
    @tipo int, 
    @usuario int
AS
BEGIN 
    DECLARE @id_new_fiscalia int
    DECLARE @fecha datetime

    INSERT INTO fiscalia(nombre, direccion, telefono, id_departamento, id_municipio, id_aldea, tipo)
    VALUES(@nombre, @direccion, @telefono, @id_departamento, @id_municipio, @id_aldea, @tipo);

    SELECT TOP 1 @id_new_fiscalia = id FROM fiscalia order by id desc;

    select @fecha = GETDATE(); 

    INSERT INTO bitacora_fiscalia(id_fiscalia, nombre, direccion, telefono, id_departamento, id_municipio, id_aldea, tipo, usuario, fecha, accion)
    VALUES(@id_new_fiscalia, @nombre, @direccion, @telefono, @id_departamento, @id_municipio, @id_aldea, @tipo, @usuario, @fecha, 'INSERT');

END



CREATE PROCEDURE actualizar_fiscalia 
    @id_fiscalia int,
    @nombre NVARCHAR(200), 
    @direccion NVARCHAR(500), 
    @telefono NVARCHAR(7), 
    @id_departamento int, 
    @id_municipio int, 
    @id_aldea int, 
    @tipo int, 
    @usuario int
AS
BEGIN 
    DECLARE @fecha datetime
    
    select @fecha = GETDATE(); 

    INSERT INTO bitacora_fiscalia(id_fiscalia, nombre, direccion, telefono, id_departamento, id_municipio, id_aldea, tipo, usuario, fecha, accion)
    SELECT id, nombre, direccion, telefono, id_departamento, id_municipio, id_aldea, tipo, @usuario, @fecha, 'UPDATE' 
        FROM fiscalia WHERE id = @id_fiscalia;

    UPDATE fiscalia SET
        nombre = @nombre,
        direccion = @direccion,
        telefono = @telefono,
        id_departamento = @id_departamento,
        id_municipio = @id_municipio,
        id_aldea = @id_aldea,
        tipo = @tipo
    WHERE id = @id_fiscalia;

END


CREATE PROCEDURE borrar_fiscalia 
    @id_fiscalia int,
    @usuario int
AS
BEGIN 
    DECLARE @fecha datetime
    
    select @fecha = GETDATE(); 

    INSERT INTO bitacora_fiscalia(id_fiscalia, nombre, direccion, telefono, id_departamento, id_municipio, id_aldea, tipo, usuario, fecha, accion)
        SELECT id, nombre, direccion, telefono, id_departamento, id_municipio, id_aldea, tipo, @usuario, @fecha, 'DELETE' 
        FROM fiscalia WHERE id = @id_fiscalia;

    DELETE FROM fiscalia
    WHERE id = @id_fiscalia;

END