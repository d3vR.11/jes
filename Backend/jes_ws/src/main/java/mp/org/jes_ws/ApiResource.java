package mp.org.jes_ws;

import com.google.gson.Gson;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import mp.org.Controller.AldeaController;
import mp.org.Controller.DepartamentoController;
import mp.org.Controller.FiscaliasController;
import mp.org.Controller.MunicipioController;
import mp.org.Controller.TipoFiscaliaController;
import mp.org.Maps.AldeaMap;
import mp.org.Maps.DepartamentoMap;
import mp.org.Maps.FiscaliaMap;
import mp.org.Maps.MunicipioMap;
import mp.org.Maps.TipoFiscaliaMap;

/**
 * REST Web Service
 *
 * @author devr
 */
@Path("api")
@RequestScoped
public class ApiResource {

    @Context
    private UriInfo context;

    public ApiResource() {
    }
    
    
    @GET()
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getFiscalias")
    public String getFiscalias() {
        FiscaliasController fiscaController = new FiscaliasController();
        return fiscaController.obtenerFiscalias();
    }
    
    @GET()
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getDepartamentos")
    public String getDepartamentos() {
        DepartamentoController deptoController = new DepartamentoController();
        return deptoController.obtenerDepartamentos();
    }
    
    @GET()
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getMunicipios")
    public String getMunicipios() {
        MunicipioController muniController = new MunicipioController();
        return muniController.obtenerMunicipios();
    }
    
    @GET()
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getAldeas")
    public String getAldeas() {
        AldeaController aldeaController = new AldeaController();
        return aldeaController.obtenerAldeas();
    }
    
    @GET()
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getTiposFiscalias")
    public String getTiposFiscalias() {
        TipoFiscaliaController tipoController = new TipoFiscaliaController();
        return tipoController.obtenerTipos();
    }
    
    @POST()
    @Produces(MediaType.APPLICATION_JSON)
    @Path("nuevaFiscalia")
    public String nuevaFiscalia(@FormParam("nombre") String nombre,
                                @FormParam("direccion") String direccion,
                                @FormParam("telefono") int telefono,
                                @FormParam("idDepartamento") int idDepartamento,
                                @FormParam("idMunicipio") int idMunicipio,
                                @FormParam("idAldea") int idAldea,
                                @FormParam("idTipo") int idTipo,
                                @FormParam("usuario") String usuario){
        FiscaliaMap fiscaliaMap = new FiscaliaMap();
        fiscaliaMap.setNombre(nombre);
        fiscaliaMap.setDireccion(direccion);
        fiscaliaMap.setTelefono(telefono);
        fiscaliaMap.setDepartamento(new DepartamentoMap(idDepartamento));
        fiscaliaMap.setMunicipio(new MunicipioMap(idMunicipio));
        fiscaliaMap.setAldea(new AldeaMap(idAldea));
        fiscaliaMap.setTipo(new TipoFiscaliaMap(idTipo));
        
        FiscaliasController fiscaController = new FiscaliasController();
        fiscaController.agregarFiscalia(fiscaliaMap, usuario);
        return new Gson().toJson("1");
    }
    
    @POST()
    @Produces(MediaType.APPLICATION_JSON)
    @Path("actualizarFiscalia")
    public String actualizarFiscalia(@FormParam("idFiscalia") int idFiscalia,
                                @FormParam("nombre") String nombre,
                                @FormParam("direccion") String direccion,
                                @FormParam("telefono") int telefono,
                                @FormParam("idDepartamento") int idDepartamento,
                                @FormParam("idMunicipio") int idMunicipio,
                                @FormParam("idAldea") int idAldea,
                                @FormParam("idTipo") int idTipo,
                                @FormParam("usuario") String usuario){
        FiscaliaMap fiscaliaMap = new FiscaliaMap();
        fiscaliaMap.setId(idFiscalia);
        fiscaliaMap.setNombre(nombre);
        fiscaliaMap.setDireccion(direccion);
        fiscaliaMap.setTelefono(telefono);
        fiscaliaMap.setDepartamento(new DepartamentoMap(idDepartamento));
        fiscaliaMap.setMunicipio(new MunicipioMap(idMunicipio));
        fiscaliaMap.setAldea(new AldeaMap(idAldea));
        fiscaliaMap.setTipo(new TipoFiscaliaMap(idTipo));
        
        FiscaliasController fiscaController = new FiscaliasController();
        fiscaController.actualizarFiscalia(fiscaliaMap, usuario);
        return new Gson().toJson("1");
    }
    
    @DELETE()
    @Produces(MediaType.APPLICATION_JSON)
    @Path("borrarFiscalia/{idFiscalia}/{usuario}")
    public String borrarFiscalia(@PathParam("idFiscalia") int idFiscalia,
                                @PathParam("usuario") String usuario){
        FiscaliasController fiscaController = new FiscaliasController();
        fiscaController.eliminarFiscalia(idFiscalia, usuario);
        return new Gson().toJson("1");
    }
    

    /**
     * PUT method for updating or creating an instance of ApiResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
