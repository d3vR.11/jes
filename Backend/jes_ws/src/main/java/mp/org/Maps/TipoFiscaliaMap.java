package mp.org.Maps;

/**
 *
 * @author devr
 */
public class TipoFiscaliaMap {
   private int id;
   private String nombre;

    public TipoFiscaliaMap() {
    }

    public TipoFiscaliaMap(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
   
   
}
