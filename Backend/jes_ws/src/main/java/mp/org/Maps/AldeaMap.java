package mp.org.Maps;

/**
 *
 * @author devr
 */
public class AldeaMap {
    private int id;
    private String nombre;
    private MunicipioMap municipio;
    private DepartamentoMap departamento;
    
    public AldeaMap(){
    }

    public AldeaMap(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public MunicipioMap getMunicipio() {
        return municipio;
    }

    public void setMunicipio(MunicipioMap municipio) {
        this.municipio = municipio;
    }

    public DepartamentoMap getDepartamento() {
        return departamento;
    }

    public void setDepartamento(DepartamentoMap departamento) {
        this.departamento = departamento;
    }
    
    
}
