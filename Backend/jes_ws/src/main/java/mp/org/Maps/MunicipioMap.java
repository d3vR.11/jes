package mp.org.Maps;

/**
 *
 * @author devr
 */
public class MunicipioMap {
    private int id;
    private String nombre;
    private DepartamentoMap departamento;

    public MunicipioMap() {
    }

    public MunicipioMap(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public DepartamentoMap getDepartamento() {
        return departamento;
    }

    public void setDepartamento(DepartamentoMap departamento) {
        this.departamento = departamento;
    }
    
    
}
