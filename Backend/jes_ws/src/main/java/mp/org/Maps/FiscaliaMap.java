package mp.org.Maps;

/**
 *
 * @author devr
 */
public class FiscaliaMap {
    private int id;
    private String nombre;
    private String direccion;
    private int telefono;
    private DepartamentoMap departamento;
    private MunicipioMap municipio;
    private AldeaMap aldea;
    private TipoFiscaliaMap tipo;

    public FiscaliaMap() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public DepartamentoMap getDepartamento() {
        return departamento;
    }

    public void setDepartamento(DepartamentoMap departamento) {
        this.departamento = departamento;
    }

    public MunicipioMap getMunicipio() {
        return municipio;
    }

    public void setMunicipio(MunicipioMap municipio) {
        this.municipio = municipio;
    }

    public AldeaMap getAldea() {
        return aldea;
    }

    public void setAldea(AldeaMap aldea) {
        this.aldea = aldea;
    }

    public TipoFiscaliaMap getTipo() {
        return tipo;
    }

    public void setTipo(TipoFiscaliaMap tipo) {
        this.tipo = tipo;
    }
    
    
}
