package mp.org.Controller;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import mp.org.EntidadesBD.Departamento;
import mp.org.EntidadesBD.Municipio;
import mp.org.Maps.DepartamentoMap;
import mp.org.Maps.MunicipioMap;
import org.javalite.activejdbc.Base;
import org.javalite.activejdbc.DB;

/**
 *
 * @author devr
 */
public class MunicipioController {

    public MunicipioController() {
        
    }
    
    
    public String obtenerMunicipios(){
        try (DB db = new DB()){
            db.open("com.microsoft.sqlserver.jdbc.SQLServerDriver", "jdbc:sqlserver://172.17.0.2:1433;databaseName=JES", "sa", "Lfm_@X6h");
            List<Municipio> munisDB = Municipio.findAll();
            List<MunicipioMap> muniMaps = new ArrayList<>();

            munisDB.stream().map((muniDB) -> {
                MunicipioMap muniMap = new MunicipioMap();
                muniMap.setId(muniDB.getInteger("id"));
                muniMap.setNombre(muniDB.getString("nombre"));

                Departamento deptoDB = Departamento.findById(muniDB.getInteger("id_departamento"));
                DepartamentoMap deptoMap = new DepartamentoMap();
                deptoMap.setId(muniDB.getInteger("id_departamento"));
                deptoMap.setNombre(deptoDB.getString("nombre"));
                muniMap.setDepartamento(deptoMap);

                return muniMap;
            }).forEachOrdered((muniMap) -> {
                muniMaps.add(muniMap);
            });
            return new Gson().toJson(muniMaps);
        }catch(Exception e){
            return "";
        }finally{
            Base.close();
        }
    }
    
}
