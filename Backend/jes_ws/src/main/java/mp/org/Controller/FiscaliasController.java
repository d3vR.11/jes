package mp.org.Controller;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import mp.org.EntidadesBD.Aldea;
import mp.org.EntidadesBD.Departamento;
import mp.org.EntidadesBD.Fiscalia;
import mp.org.EntidadesBD.Municipio;
import mp.org.EntidadesBD.TipoFiscalia;
import mp.org.Maps.AldeaMap;
import mp.org.Maps.DepartamentoMap;
import mp.org.Maps.FiscaliaMap;
import mp.org.Maps.MunicipioMap;
import mp.org.Maps.TipoFiscaliaMap;
import org.javalite.activejdbc.Base;
import org.javalite.activejdbc.DB;

/**
 *
 * @author devr
 */
public class FiscaliasController {

    public FiscaliasController() {
        
    }
    
    public String obtenerFiscalias(){
        try (DB db = new DB()){
            db.open("com.microsoft.sqlserver.jdbc.SQLServerDriver", "jdbc:sqlserver://172.17.0.2:1433;databaseName=JES", "sa", "Lfm_@X6h");
            List<Fiscalia> fiscaliasDB = Fiscalia.findAll();
            List<FiscaliaMap> fiscaliasMap = new ArrayList<>();

            fiscaliasDB.stream().map((fiscaliaDB) -> {
                FiscaliaMap fiscaliaMap = new FiscaliaMap();
                fiscaliaMap.setId(fiscaliaDB.getInteger("id"));
                fiscaliaMap.setNombre(fiscaliaDB.getString("nombre"));
                fiscaliaMap.setDireccion(fiscaliaDB.getString("direccion"));
                fiscaliaMap.setTelefono(Integer.parseInt(fiscaliaDB.getString("telefono")));

                Aldea aldeaDB = Aldea.findById(fiscaliaDB.getInteger("id_aldea"));
                AldeaMap aldeaMap = new AldeaMap();
                aldeaMap.setId(fiscaliaDB.getInteger("id_aldea"));
                aldeaMap.setNombre(aldeaDB.getString("nombre"));
                fiscaliaMap.setAldea(aldeaMap);

                Municipio muniDB = Municipio.findById(fiscaliaDB.getInteger("id_municipio"));
                MunicipioMap muniMap = new MunicipioMap();
                muniMap.setId(fiscaliaDB.getInteger("id_municipio"));
                muniMap.setNombre(muniDB.getString("nombre"));
                fiscaliaMap.setMunicipio(muniMap);

                Departamento deptoDB = Departamento.findById(fiscaliaDB.getInteger("id_departamento"));
                DepartamentoMap deptoMap = new DepartamentoMap();
                deptoMap.setId(fiscaliaDB.getInteger("id_departamento"));
                deptoMap.setNombre(deptoDB.getString("nombre"));
                fiscaliaMap.setDepartamento(deptoMap);

                TipoFiscalia tipoDB = TipoFiscalia.findById(fiscaliaDB.getInteger("tipo"));
                TipoFiscaliaMap tipoMap = new TipoFiscaliaMap();
                tipoMap.setId(fiscaliaDB.getInteger("tipo"));
                tipoMap.setNombre(tipoDB.getString("nombre"));
                fiscaliaMap.setTipo(tipoMap);

                return fiscaliaMap;
            }).forEachOrdered((fiscaliaMap) -> {
                fiscaliasMap.add(fiscaliaMap);
            });
            
            return new Gson().toJson(fiscaliasMap);
        }catch(Exception e){
            return "";
        }finally{
            Base.close();
        }
    }
    
    public void agregarFiscalia(FiscaliaMap fiscalia, String usuario){
        try (DB db = new DB()){
            db.open("com.microsoft.sqlserver.jdbc.SQLServerDriver", "jdbc:sqlserver://172.17.0.2:1433;databaseName=JES", "sa", "Lfm_@X6h");
            String exec = String.format("EXEC nueva_fiscalia '%s', '%s', %s, %d, %d, %d, %d, %s",
                    fiscalia.getNombre(),
                    fiscalia.getDireccion(),
                    String.valueOf(fiscalia.getTelefono()),
                    fiscalia.getDepartamento().getId(),
                    fiscalia.getMunicipio().getId(),
                    fiscalia.getAldea().getId(),
                    fiscalia.getTipo().getId(),
                    usuario);
            Base.exec(exec);
        }catch(Exception e){
        }finally{
            Base.close();
        }
        
    }
    
    public void actualizarFiscalia(FiscaliaMap fiscalia, String usuario){
        try (DB db = new DB()){
            db.open("com.microsoft.sqlserver.jdbc.SQLServerDriver", "jdbc:sqlserver://172.17.0.2:1433;databaseName=JES", "sa", "Lfm_@X6h");
            String exec = String.format("EXEC actualizar_fiscalia %d, '%s', '%s', %s, %d, %d, %d, %d, %s",
                    fiscalia.getId(),
                    fiscalia.getNombre(),
                    fiscalia.getDireccion(),
                    String.valueOf(fiscalia.getTelefono()),
                    fiscalia.getDepartamento().getId(),
                    fiscalia.getMunicipio().getId(),
                    fiscalia.getAldea().getId(),
                    fiscalia.getTipo().getId(),
                    usuario);
            Base.exec(exec);
        }catch(Exception e){
        }finally{
            Base.close();
        }
        
    }
    
    public void eliminarFiscalia(int idFiscalia, String usuario){
        try (DB db = new DB()){
            db.open("com.microsoft.sqlserver.jdbc.SQLServerDriver", "jdbc:sqlserver://172.17.0.2:1433;databaseName=JES", "sa", "Lfm_@X6h");
            String exec = String.format("EXEC borrar_fiscalia %d, %s",
                    idFiscalia,
                    usuario);
            Base.exec(exec);
        }catch(Exception e){
        }finally{
            Base.close();
        }
        
    }
}
