package mp.org.Controller;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import mp.org.EntidadesBD.Departamento;
import mp.org.Maps.DepartamentoMap;
import org.javalite.activejdbc.Base;
import org.javalite.activejdbc.DB;

/**
 *
 * @author devr
 */
public class DepartamentoController {

    public DepartamentoController() {
        
    }
    
    public String obtenerDepartamentos(){
        try (DB db = new DB()){
            db.open("com.microsoft.sqlserver.jdbc.SQLServerDriver", "jdbc:sqlserver://172.17.0.2:1433;databaseName=JES", "sa", "Lfm_@X6h");
            List<Departamento> departamentosDB = Departamento.findAll();
            List<DepartamentoMap> departamentosMap = new ArrayList<>();

            departamentosDB.stream().map((deptoBD) -> {
                DepartamentoMap deptoMap = new DepartamentoMap();
                deptoMap.setId(deptoBD.getInteger("id"));
                deptoMap.setNombre(deptoBD.getString("nombre"));
                return deptoMap;
            }).forEachOrdered((deptoMap) -> {
                departamentosMap.add(deptoMap);
            });

            return new Gson().toJson(departamentosMap);
        }catch(Exception e){
            return "";
        }finally{
            Base.close();
        }   
    }
}
