package mp.org.Controller;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import mp.org.EntidadesBD.Aldea;
import mp.org.EntidadesBD.Departamento;
import mp.org.EntidadesBD.Municipio;
import mp.org.Maps.AldeaMap;
import mp.org.Maps.DepartamentoMap;
import mp.org.Maps.MunicipioMap;
import org.javalite.activejdbc.Base;
import org.javalite.activejdbc.DB;

/**
 *
 * @author devr
 */
public class AldeaController {

    public AldeaController() {
        
    }
    
    public String obtenerAldeas(){
        try (DB db = new DB()){
            db.open("com.microsoft.sqlserver.jdbc.SQLServerDriver", "jdbc:sqlserver://172.17.0.2:1433;databaseName=JES", "sa", "Lfm_@X6h");
            List<Aldea> aldeasDB = Aldea.findAll();
            List<AldeaMap> aldeasMap = new ArrayList<>();

            aldeasDB.stream().map((aldeaDB) -> {
                AldeaMap aldeaMap = new AldeaMap();
                aldeaMap.setId(aldeaDB.getInteger("id"));
                aldeaMap.setNombre(aldeaDB.getString("nombre"));

                Municipio muniDB = Municipio.findById(aldeaDB.getInteger("id_municipio"));
                MunicipioMap muniMap = new MunicipioMap();
                muniMap.setId(aldeaDB.getInteger("id_municipio"));
                muniMap.setNombre(muniDB.getString("nombre"));
                aldeaMap.setMunicipio(muniMap);

                Departamento deptoDB = Departamento.findById(aldeaDB.getInteger("id_departamento"));
                DepartamentoMap deptoMap = new DepartamentoMap();
                deptoMap.setId(aldeaDB.getInteger("id_departamento"));
                deptoMap.setNombre(deptoDB.getString("nombre"));
                aldeaMap.setDepartamento(deptoMap);

                return aldeaMap;
            }).forEachOrdered((aldeaMap) -> {
                aldeasMap.add(aldeaMap);
            });
            return new Gson().toJson(aldeasMap);
        }catch(Exception e){
            return "";
        }finally{
            Base.close();
        }
    }
}
