package mp.org.Controller;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import mp.org.EntidadesBD.TipoFiscalia;
import mp.org.Maps.TipoFiscaliaMap;
import org.javalite.activejdbc.Base;
import org.javalite.activejdbc.DB;

/**
 *
 * @author devr
 */
public class TipoFiscaliaController {

    public TipoFiscaliaController() {
    }
    
    public String obtenerTipos(){
        try (DB db = new DB()){
            db.open("com.microsoft.sqlserver.jdbc.SQLServerDriver", "jdbc:sqlserver://172.17.0.2:1433;databaseName=JES", "sa", "Lfm_@X6h");
            List<TipoFiscalia> tiposDB = TipoFiscalia.findAll();
            List<TipoFiscaliaMap> tiposMap = new ArrayList<>();

            tiposDB.stream().map((tipoDB) -> {
                TipoFiscaliaMap tipoMap = new TipoFiscaliaMap();
                tipoMap.setId(tipoDB.getInteger("id"));
                tipoMap.setNombre(tipoDB.getString("nombre"));

                return tipoMap;
            }).forEachOrdered((tipoMap) -> {
                tiposMap.add(tipoMap);
            });

            return new Gson().toJson(tiposMap);
        }catch(Exception e){
            return "";
        }finally{
            Base.close();
        }
    }
}
